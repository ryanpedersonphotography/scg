<?php

	/*--------------------
	 BILLBOARD VARIABLES
	--------------------*/

	/* Headlines */
	$head1 = "Introducing 3030 Aria";
	$head2 = "Eclipse Titanium Figure Skate Blades";
	$head3 = "What's your Riedell story?";
	$head4 = "Coach Spotlight";
  $head5 = "Introducing Eclipse Quantum";


	/* Copy */
	$copy1 = "Feel as light as air with our all new Aria and Eclipse Titanium Pinnacle Blades";
	$copy2 = "Introducing the lightest blades on the market. They combine lightness and elegance with strength and performance to help you reach your skating dreams.";
	$copy3 = "If you count on Riedell ice skates to soar to new heights we'd love to hear your story!";
	$copy4 = "&ldquo;I worship Riedell. They make the best boots out there.&rdquo;";
  $copy5 = "With the Quantum Blade we did the math so that you can defy gravity";

	/* Button Labels */
	$btn1 = "Learn More";
	$btn2 = "Learn More";
	$btn3 = "Share Your Story";
	$btn4 = "Team Riedell";
  $btn5 = "Learn More";

	/* Button Links */
	$link1 = "figure-skate-boots/aria/";
	$link2 = "2013/introducing-eclipse-titanium/";
	$link3 = "tell-your-story/";
	$link4 = "2012/coach-spotlight-thomas-amon-psa-master-rated-coach/";
  $link5 = "http://ice.riedellskates.com/products/blades/quantum/#.V7IuZ2VUNVo";


	/* Image URIs */
	$img1 = "wp-content/uploads/2014/01/intro-aria-figure-skate-boot-billboard.jpg";
	$img2 = "wp-content/uploads/2013/05/intro-eclipse-titanium-billboard.jpg";
	$img3 = "wp-content/uploads/2012/10/rachael-flatt-professional-figure-skater.jpg";
	$img4 = "wp-content/uploads/2012/10/thomas-amon-figure-skating-coach.jpg";


?>
<div id="slider_wrapper"> <span id="slider_wrapper"> </span>
  <div id="nivo_caption_wrapper">
  	<span id="slider_wrapper">
    </span>
    </span>
    <br />
    <br />
    <br />



    <div id="caption1" class="nivo-html-caption" style="display:block">
      <h4><?php echo $head1; ?></h4>
      <br />
      <?php echo $copy1; ?><br />
      <br />
      <a href="<?php echo $link1; ?>" class="button-small" style="margin-top:0;"><?php echo $btn1; ?></a>
    </div>
    </span>


    <div id="caption2" class="nivo-html-caption">
      <h4><?php echo $head2; ?></h4>
      <br />
      <?php echo $copy2; ?><br />
      <br />
      <a href="<?php echo $link2; ?>" class="button-small" style="margin-top:0;"><?php echo $btn2; ?></a>
    </div>
    </span>

    <div id="caption3" class="nivo-html-caption">
      <h4><?php echo $head3; ?></h4>
      <br />
      <?php echo $copy3; ?><br />
      <br />
      <a href="<?php echo $link3; ?>" class="button-small" style="margin-top:0;"><?php echo $btn3; ?></a>
    </div>
    </span>

    <div id="caption4" class="nivo-html-caption">
      <h4><?php echo $head4; ?></h4>
      <br />
      <?php echo $copy4; ?><br />
      <br />
      <a href="<?php echo $link4; ?>" class="button-small" style="margin-top:0;"><?php echo $btn4; ?></a>
    </div>

    <div id="caption5" class="nivo-html-caption" style="display:block">
      <h4><?php echo $head5; ?></h4>
      <br />
      <?php echo $copy5; ?><br />
      <br />
      <a href="<?php echo $link5; ?>" class="button-small" style="margin-top:0;"><?php echo $btn1; ?></a>
    </div>
    </span>


  </div>
  <div id="nivo_slider" class="nivoSlider">

    <a href="#">
    <img src="wp-content/themes/iskateriedell/timthumb.php?src=http://www.iskateriedell.com/wp-content/uploads/2016/05/riedell-600x298-b.jpg" alt="#caption5" rel="http://www.iskateriedell.com/wp-content/uploads/2016/05/eclipse_blades-60x60.jpg"/></a>
    <a href="#">
    <?php /*<div class="ujiCountdown ujic_center" id="ujiCountdown1"></div>*/ ?>
    <img src="wp-content/themes/iskateriedell/timthumb.php?src=<?php echo $img1; ?>&amp;h=298&amp;w=600&amp;zc=1" alt="#caption0" rel="wp-content/themes/iskateriedell/timthumb.php?src=<?php echo $img1; ?>&amp;h=60&amp;w=60&amp;zc=1"/></a>

    <a href="#">
    <img src="wp-content/themes/iskateriedell/timthumb.php?src=<?php echo $img2 ?>&amp;h=298&amp;w=600&amp;zc=1" alt="#caption1" rel="wp-content/themes/iskateriedell/timthumb.php?src=<?php echo $img2; ?>&amp;h=60&amp;w=60&amp;zc=1"/></a>

    <a href="#">
    <img src="wp-content/themes/iskateriedell/timthumb.php?src=<?php echo $img3; ?>&amp;h=298&amp;w=600&amp;zc=1" alt="#caption2" rel="wp-content/themes/iskateriedell/timthumb.php?src=<?php echo $img3; ?>&amp;h=60&amp;w=60&amp;zc=1"/></a>

    <a href="#">
    <img src="wp-content/themes/iskateriedell/timthumb.php?src=<?php echo $img4; ?>&amp;h=298&amp;w=600&amp;zc=1" alt="#caption3" rel="wp-content/themes/iskateriedell/timthumb.php?src=<?php echo $img4; ?>&amp;h=60&amp;w=60&amp;zc=1"/></a>

   </div>
</div>
